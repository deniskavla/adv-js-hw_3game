

class Game {
    constructor() {
        this.arrActivTd = Array(100);
        this.$active = false;
        this.countUsers = 0;
        this.countComp = 0;
        this.timerId = false;
        this.time = false;
        $('.easy').on('click', (e) => {
            this.time = 1500;
            this.makeTurn();
        });
        $('.normal').on('click', (e) => {
            this.time = 1000;
            this.makeTurn();
        });
        $('.hard').on('click', (e) => {
            this.time = 500;
            this.makeTurn();
        });
        $('td').on('click', (e) => {
            if ($(e.target).is(this.$active)) {
                this.markAsUserCell();
                clearTimeout(this.timerId);
                this.makeTurn();
            }
        });
        $('.reset').on('click', (e) => {
            clearTimeout(this.timerId);
            $("table td").removeClass("active user comp");
        });

    }
    generateRandom() {
        let num = Math.floor(1 + Math.random() * this.arrActivTd.length);
        if (this.arrActivTd.includes(num)) {
            return this.generateRandom();
        } else {
            this.arrActivTd[num] = num;
            return num;
        }
    }
    pickRandomCell() {
        let numRandom = this.generateRandom();
        let idNumRandom = "#" + numRandom;
        this.$active = $(idNumRandom);
        this.$active.addClass('active');
    }
     markAsUserCell() {
        if (this.$active) {
            this.$active.removeClass('active');
            this.$active.addClass('user');
            this.countUsers++;
            this.$active = null;
        }
    }
    markAsCompCell() {
        if (this.$active) {
            this.$active.removeClass('active');
            this.$active.addClass('comp');
            this.countComp++;
            this.active = null;
        }
    }
    makeTurn() {
        this.pickRandomCell();
        this.timerId = setTimeout(() => {
            this.markAsCompCell();
            this.makeTurn();
        }, this.time);
        if (this.countUsers >= 50) {
            clearTimeout(this.timerId);
            alert('Users Win');
        }
        if (this.countComp >= 50) {
            clearTimeout(this.timerId);
            alert('Comp Win');
        }
    }
}


new Game();



// const arrActivTd = Array();
// let $active;
// let countUsers = 0;
// let countComp = 0;
// let timerId;
// let time;
//
// function generateRandom() {
//     let num = Math.floor(1 + Math.random() * arrActivTd.length);
//     if (arrActivTd.includes(num)) {
//         return generateRandom();
//     } else {
//         arrActivTd[num] = num;
//         return num;
//     }
// }
//
// function pickRandomCell() {
//     let numRandom = generateRandom();
//     let idNumRandom = "#" + numRandom;
//     $active = $(idNumRandom);
//     $active.addClass('active');
// }
//
// function markAsUserCell() {
//     if ($active) {
//         $active.removeClass('active');
//         $active.addClass('user');
//         countUsers++;
//         $active = null;
//     }
// }
//
// function markAsCompCell() {
//     if ($active) {
//         $active.removeClass('active');
//         $active.addClass('comp');
//         countComp++;
//         $active = null;
//     }
// }
//
// $('td').on('click', (e) => {
//     if ($(e.target).is($active)) {
//         markAsUserCell();
//         clearTimeout(timerId);
//         makeTurn();
//     }
// });
//
// function makeTurn() {
//     pickRandomCell();
//     timerId = setTimeout(() => {
//         markAsCompCell();
//         makeTurn();
//     }, time);
//     if (countUsers >= 50) {
//         clearTimeout(timerId);
//         alert('Users Win');
//     }
//     if (countComp >= 50) {
//         clearTimeout(timerId);
//         alert('Comp Win');
//     }
// }
//
// $('.reset').on('click', (e) => {
//     clearTimeout(timerId);
//     $("table td").removeClass("active user comp");
// });
//
// $('.easy').on('click', (e) => {
//     time = 1500;
//     makeTurn();
// });
// $('.normal').on('click', (e) => {
//     time = 1000;
//     makeTurn();
// });
// $('.hard').on('click', (e) => {
//     time = 500;
//     makeTurn();
// });